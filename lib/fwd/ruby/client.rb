module Forward

	class Client

		def initialize(host, port, login = nil)
			# port is optional

			if (login.nil? && port.is_a?(Hash))
				login = port
				port = nil
			end

			@host = host
			@port = port || 22937
			@login = login

			connect()
		end

		def connect
			@server = Connection.new(@host, @port)
			auth(@login) unless @login == nil
		end #connect

		def call(method, args = Array.new)
			result = @server.call(method, args)
			result
		end # call

		def get(uri, data = nil)
			call("get", [uri, data])
			# TODO: retry on protocol exception
		end # get

		def put(uri, data = nil)
			call("put", [uri, data])
		end # put

		def post(uri, data = nil)
			call("post", [uri, data])
		end # post

		def delete(uri, data = nil)
			call("delete", [uri, data])
		end # delete

		def auth(params)
			# Get nonce
			auth = call("auth")

			# Create key hash
			key_hash = Digest::MD5.hexdigest("#{params[:client]}:fwd:#{params[:key]}")

			# Create auth key
			auth_key = Digest::MD5.hexdigest("#{auth['nonce']}#{params[:client]}#{key_hash}")

			# Authenticate with login creds
			creds = {
				"client" => params[:client],
				"nonce" => auth['nonce'],
				"key" => auth_key,
				"session" => params[:session]
			}

			result = call("auth", [creds])

			# Server should return session id
			if session_id = result
				return session_id
			end

			raise ClientException, "Authentication failed (user: #{params[:client]}, key: #{params[:key]})"
		end # auth

	end # Client

	#
	# Client exceptions
	#
	class ClientException < Exception
	end

	#
	# Thrown on network errors
	#
	class NetworkException < ClientException
	end

	#
	# Thrown on remote protocol errors
	#
	class ProtocolException < ClientException
	end

	#
	# Thrown on server errors
	#
	class ServerException < ClientException
	end

	#
	# Main class which handles API communction
	#
	class Connection

		require "socket"
		require "json"
		require "active_support"

		@@callbackNumber = 40;
		@@stream = nil

		def initialize(host, port)

			@@stream = TCPSocket.new host, port

			if @@stream
				## TODO
			else
			 	raise NetworkException, "Server Not found"
			end
		end #initialize

		def call(method, args = Array.new)

			raise NetworkException, "Unable to execute(#{method}). Server is closes" if @@stream.nil?

			callbacks = Hash.new
			@@callbackNumber += 1
			callbacks[@@callbackNumber] = Array[args.size]

			json_request =  JSON.generate(Array[method, args, callbacks])
			@@stream.write(json_request + "\n")

			## Block until server respondsc
			if false == (response = @@stream.gets)
				close()
				raise ProtocolException, "Unable to read from server"
			end

			if nil == (message = ActiveSupport::JSON.decode(response.strip))
				raise ProtocolException, "Unable to parse response from server #{message}"
			elsif !message.kind_of?(Array)
				raise ProtocolException, "Invalid response from server (#{$message})"
			elsif message[2].present?
				message = message[2].is_a?(String) ? message[2] : message[2].to_s
				raise ServerException message
			end

			return message[1]
		end # call

		def close
			@@stream.close
	    	@@stream = nil;
		end # close

	end # Connection

end
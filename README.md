# Forward // Client library for Ruby

__Connect to the Forward API server using Ruby__

## Usage example

	require 'forward'

	fwd = Forward::Client.new("dev.getfwd.com", 22937, {
		client: "eric",
		key: "abc123"
	})

	data = fwd.get("/payments");

	products = fwd.get("/products", {'color': "blue"})

	print products.inspect

## Documentation

See <http://getfwd.com/docs/clients#ruby> for more API docs and usage examples

## Contributing

Pull requests are welcome

## License

MIT
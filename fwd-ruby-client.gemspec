# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fwd/ruby/client/version'

Gem::Specification.new do |spec|
  spec.name          = "fwd-ruby-client"
  spec.version       = Fwd::Ruby::Client::VERSION
  spec.authors       = ["Eric Ingram"]
  spec.email         = ["eric@getfwd.com"]
  spec.description   = %q{
    Forward is the platform for creative e-commerce.
    This client library provides easy access to the Forward API using Python.
    }
  spec.summary       = %q{Forward client library for Ruby}
  spec.homepage      = "https://getfwd.com/libs#ruby"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
